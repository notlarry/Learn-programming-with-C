#include <stdio.h>

    /* print Celsus-Fahrenheit table for celsus = 0, 20, ..., 300; floating-point version */
int main()
{
    float fahr, celsius;
    int lower, upper, step;

    lower = 0;    /* lower limit of temperature table */
    upper = 300;  /* upper limit of temperature table */
    step = 20;    /* step size */
    celsius = lower; /* assign the value of lower to celsius */
    
    printf("Celsius\tFahren\n");
    while (celsius <= upper) {   
        fahr = (celsius * (9.0/5.0) + 32);
        printf("%6.0f\t%3.1f\n", celsius, fahr);
        celsius = celsius + step;
    }
} 
