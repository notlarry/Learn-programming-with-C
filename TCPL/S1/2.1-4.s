	.file	"2.1-4.c"
	.section	.rodata
.LC0:
	.string	"Fahr:\tCelsius"
.LC1:
	.string	"_______________"
.LC4:
	.string	"%6.0f\t%3.1f\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	$0, -8(%rbp)
	movl	$300, -12(%rbp)
	movl	$20, -16(%rbp)
	pxor	%xmm0, %xmm0
	cvtsi2ss	-8(%rbp), %xmm0
	movss	%xmm0, -20(%rbp)
	movl	$.LC0, %edi
	call	puts
	movl	$.LC1, %edi
	call	puts
	jmp	.L2
.L3:
	cvtss2sd	-20(%rbp), %xmm0
	movsd	.LC2(%rip), %xmm1
	subsd	%xmm1, %xmm0
	movsd	.LC3(%rip), %xmm1
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm2
	movss	%xmm2, -4(%rbp)
	cvtss2sd	-20(%rbp), %xmm1
	cvtss2sd	-4(%rbp), %xmm0
	movl	$.LC4, %edi
	movl	$2, %eax
	call	printf
	pxor	%xmm0, %xmm0
	cvtsi2ss	-16(%rbp), %xmm0
	movss	-4(%rbp), %xmm1
	addss	%xmm1, %xmm0
	movss	%xmm0, -4(%rbp)
.L2:
	pxor	%xmm0, %xmm0
	cvtsi2ss	-12(%rbp), %xmm0
	ucomiss	-4(%rbp), %xmm0
	jnb	.L3
	movl	$0, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.section	.rodata
	.align 8
.LC2:
	.long	0
	.long	1077936128
	.align 8
.LC3:
	.long	1908874354
	.long	1071761180
	.ident	"GCC: (GNU) 5.4.0"
	.section	.note.GNU-stack,"",@progbits
